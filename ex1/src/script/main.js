let temps = 0
const timerElement = document.getElementById("timer")
timerElement.innerText = `${0}:${0}`
let started = false;

let start = document.querySelector("#start");

start.addEventListener('click', ()=>{started = true
})

let stop = document.querySelector("#stop");

stop.addEventListener('click', ()=>{started = false})

let restart = document.querySelector("#restart");

restart.addEventListener('click', ()=>{
    temps = 0
    timerElement.innerText = `${0}:${0}`
        })

setInterval(() => {
    let minute = parseInt(temps/60, 10)
    let secondes = parseInt(temps % 60, 10)
          
    minute = minute < 10 ? "0" + minute : minute
    secondes = secondes < 10 ? "0" + secondes : secondes
          
    timerElement.innerText = `${minute}:${secondes}`
    temps = started? temps = temps + 1 : temps
}, 1000)
